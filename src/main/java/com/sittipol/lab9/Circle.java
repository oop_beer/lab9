package com.sittipol.lab9;

public class Circle extends Shape {
    private double radius;
    public Circle(double radius){
        super("Circle");
        this.radius =radius;
    }
    @Override
    public double calArea() {
        // TODO Auto-generated method stub
        return Math.PI * this.radius*this.radius;
    }
    @Override
    public double calPerimeter() {
        // TODO Auto-generated method stub
        return 2*Math.PI*this.radius;
    }
    @Override
    public String toString() {
        return this.getName() + " radius: " + this.radius ;
    }
}
